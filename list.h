#pragma once
#include <memory>

struct Nodo;
/**
 * @brief Estructura que almacena los datos de cada recordatorio, junto a la direccion del anterior y el siguiente
 *
 */
struct Nodo
{
    int ID;
    std::string nota;
    std::string fecha;
    std::string hora;
    std::shared_ptr<Nodo> nodo_next = nullptr;
    std::shared_ptr<Nodo> nodo_previous = nullptr;
};

/**
 * @brief Funcion para insertar datos a la lista
 *
 * @param lista Lista de recordatorios
 * @param id ID del recordatorio
 * @param nota Recordatorio
 * @param fecha Fecha
 * @param hora Hora
 */
void insertar_dato(std::shared_ptr<Nodo> &lista, int id, std::string nota, std::string fecha, std::string hora);

/**
 * @brief Funcion para mostrar la recordatorio de la lista
 *
 * @param lista Lista de recordatorios
 * @param id ID del recordatorio a mostrar
 */
int buscar_dato(std::shared_ptr<Nodo> &lista, int id);

/**
 * @brief
 *
 * @param lista Funcion para eliminar un recordatorio de la lista
 * @param id ID del recordatorio a eliminar
 * @return true En caso de haber logrado borrar el recordatorio
 * @return false En caso de no encontrar el recordatorio
 */
bool eliminar_dato(std::shared_ptr<Nodo> &lista, int id);

/**
 * @brief Funcion para mostrar todos los recordatorios de la lista
 *
 * @param lista Lista de recordatorios
 */
void mostrar_datos(const std::shared_ptr<Nodo> &lista);


// asd