/*
1) Haciendo uso de smart pointers, cree una estructura llamada Cámara, Cámara tiene cantidad
de disparos, marca y año de fabricación.
a) Cree una función que reciba como parámetro un puntero a la estructura Cámara. La
función verificará si la cantidad de disparos ha alcanzado el límite permitido, si es
así, entonces eliminará la estructura mediante reset.
b) Mediante un ciclo ‘while’ aumente la cantidad de disparos de la Cámara y llame a la
función de verificación.
c) Para salir del ciclo ‘while’, debe verificar que la cámara ya ha sido destruida, esto lo
puede verificar mediante el uso de un weak pointer.
*/
#include <iostream>
#include <memory>
#include <string>

struct Camara
{
    int disparos;
    int limite_disparos;
    std::string marca;
    int anho;
};

bool verificar(std::shared_ptr<Camara> tcamera)
{
    if (tcamera->disparos == tcamera->limite_disparos)
    {
        tcamera.reset();
        std::cout << "Camara destruida" << std::endl;
        return true;
    }
    else
    {
        std::cout << "Quedan " << tcamera->limite_disparos - tcamera->disparos << " disparos disponibles" << std::endl;
        return false;
    }
}

void insertar_datos_camera(std::shared_ptr<Camara> tcamera, int tdisparos, int tlimite, std::string tmarca, int tanho)
{

    tcamera->disparos = tdisparos;
    tcamera->limite_disparos = tlimite;
    tcamera->marca = tmarca;
    tcamera->anho = tanho;

    std::cout << "Marca de la camara: " << tcamera->marca << std::endl;
    std::cout << "Limite de disparos: " << tcamera->limite_disparos << std::endl;
    std::cout << "Anho de la camara: " << tcamera->anho << std::endl;
}

int main()
{
    std::shared_ptr<Camara> newCamera = std::make_shared<Camara>();
    insertar_datos_camera(newCamera, 0, 10, "Sony", 1999);
    bool limite = false;

    while (limite != true)
    {
        std::cout << "Tomando fotografia" << std::endl;
        newCamera->disparos++;
        std::cout << newCamera->disparos << std::endl;
        std::cout << newCamera->limite_disparos << std::endl;
        limite = verificar(newCamera);
    }
    std::cout << "Ciclo finalizado" << std::endl;
    return 0;
}