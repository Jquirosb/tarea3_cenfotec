#include <iostream>
#include <memory>
#include <stdlib.h>
//#include "list.h"
#include "list.cpp"

int main()
{
    std::shared_ptr<Nodo> lista = std::make_shared<Nodo>();
    //
    bool ciclo = true;
    char opcion;
    int tID;
    int contador = 0;
    std::string tnota;
    std::string tfecha;
    std::string thora;
    while (opcion != 'Q')
    {
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << "Digite el numero de la funcion que desea utilizar" << std::endl;
        std::cout << "1. Crear recordatorio" << std::endl;
        std::cout << "2. Mostrar lista de recordatorios" << std::endl;
        std::cout << "3. Buscar recordatorio por ID" << std::endl;
        std::cout << "4. Eliminar recordatorio por ID" << std::endl;
        std::cout << "Escriba la Q para cerrar el programa" << std::endl;
        std::cout << "---------------------------------------------------" << std::endl;
        std::cin >> opcion;
        switch (opcion)
        {
        case '1':
            // system ("CLS");
            std::cout << "Inserte una ID para el recordatorio: " << std::endl;
            std::cin >> tID;
            std::cout << "Escriba la nota que desea guardar: " << std::endl;
            std::cin >> tnota;
            std::cout << "Inserte la fecha: " << std::endl;
            std::cin >> tfecha;
            std::cout << "Inserte la hora: " << std::endl;
            std::cin >> thora;
            insertar_dato(lista, tID, tnota, tfecha, thora);
            if (contador == 0)
            { // Al inicializa la lista se crea un recordatorio por default, entonces esto para eliminarlo
                eliminar_dato(lista, 0);
            }
            contador++;
            break;
        case '2':
            // system ("CLS");
            if (contador == 0)
            {
                std::cout << "No hay recordatorios guardados." << std::endl;
                break;
            }
            std::cout << "Mostrando la lista. " << std::endl;
            mostrar_datos(lista);
            break;
        case '3':
            // system ("CLS");
            std::cout << "Ingrese la ID a buscar. " << std::endl;
            std::cin >> tID;
            buscar_dato(lista, tID);
            break;
        case '4':
            // system ("CLS");
            std::cout << "Ingrese la ID del recordatorio a eliminar. " << std::endl;
            std::cin >> tID;
            eliminar_dato(lista, tID);
            break;
        case 'Q':
            // system ("CLS");
            std::cout << "Cerrando programa" << std::endl;
            break;
        default:
            // system ("CLS");
            std::cout << "Opcion invalida" << std::endl;
            break;
        }
    }

    return 0;
}

// asd