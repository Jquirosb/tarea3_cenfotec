#include <iostream>
#include "list.h"

void insertar_dato(std::shared_ptr<Nodo> &lista, int id, std::string nota, std::string fecha, std::string hora)
{
    if (lista == nullptr)
    { // En caso de que sea el primer elemento de

        return;
    }

    std::shared_ptr<Nodo> nodo_temp = lista;

    while (nodo_temp->nodo_next != nullptr)
    {
        nodo_temp = nodo_temp->nodo_next;
        std::cout << "vamos por el dato " << nodo_temp->ID << std::endl;
    }

    nodo_temp->nodo_next = std::make_shared<Nodo>();
    nodo_temp->nodo_next->nodo_previous = nodo_temp;
    nodo_temp->nodo_next->ID = id;
    nodo_temp->nodo_next->nota = nota;
    nodo_temp->nodo_next->fecha = fecha;
    nodo_temp->nodo_next->hora = hora;

    std::cout << "ID del dato ingresado: " << nodo_temp->nodo_next->ID << std::endl;
    std::cout << "Nota ingresada: " << nodo_temp->nodo_next->nota << std::endl;
    std::cout << "Fecha: " << nodo_temp->nodo_next->fecha << std::endl;
    std::cout << "Hora:  " << nodo_temp->nodo_next->hora << std::endl;
}

int buscar_dato(std::shared_ptr<Nodo> &lista, int id)
{
    if (lista == nullptr)
    {
        std::cout << "La lista esta vacia" << std::endl;
        return 0;
    }

    std::shared_ptr<Nodo> nodo_temp = lista;

    while (nodo_temp->nodo_next != nullptr)
    {
        if (nodo_temp->ID == id)
        {
            std::cout << "La nota en el ID " << nodo_temp->ID << " es: " << std::endl;
            std::cout << nodo_temp->nota << std::endl;
            return 0;
        }
        nodo_temp = nodo_temp->nodo_next;
    }
    return 0;
}

bool eliminar_dato(std::shared_ptr<Nodo> &lista, int id)
{
    if (lista == nullptr)
    {
        std::cout << "[Error] La lista esta vacia" << std::endl;
        return false;
    }
    std::shared_ptr<Nodo> anterior = lista;
    std::shared_ptr<Nodo> actual = lista->nodo_next;

    if (anterior->ID == id)
    {
        lista = actual;
        lista->nodo_previous = anterior->nodo_previous;
        anterior.reset();
        std::cout << "funciono " << std::endl;
        return true;
    }
    while (actual != nullptr)
    {
        if (actual->ID == id)
        {
            anterior->nodo_next = actual->nodo_next;
            if (actual->nodo_next != nullptr)
            {
                actual->nodo_next->nodo_previous = anterior;
            }
            actual.reset();
            std::cout << "Se ha borrado el recordatorio con ID: " << id << std::endl;
            return true;
        }
        anterior = actual;
        actual = actual->nodo_next;
    }
    std::cout << "No se encontro el recordatorio con ID: " << id << std::endl;
    return false;
}

void mostrar_datos(const std::shared_ptr<Nodo> &lista)
{
    std::shared_ptr<Nodo> actual = lista;
    int indice = 0;
    if (actual == nullptr)
    {
        std::cout << "No hay datos en la lista" << std::endl;
    }
    while (actual != nullptr)
    {
        std::cout << "Dato en ID " << actual->ID << ": " << std::endl;
        std::cout << "Nota ingresada: " << actual->nota << std::endl;
        std::cout << "Fecha: " << actual->fecha << std::endl;
        std::cout << "Hora:  " << actual->hora << std::endl;

        actual = actual->nodo_next;
        indice++;
    }
}

// asd